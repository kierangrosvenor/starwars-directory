import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import StarshipsIndex from './views/starships/index'
import PlanetsIndex from './views/planets/index'
import CharactersIndex from './views/characters/index'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path: '/starships',
            name: 'starships',
            component: StarshipsIndex
        },
        {
            path: '/planets',
            name: 'planets',
            component: PlanetsIndex,
        },
        {
            path: '/characters',
            name: 'characters',
            component: CharactersIndex
        },
        {
            path: '*',
            redirect: '/home'
        }
    ]
})
