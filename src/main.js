import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueI18n from 'vue-i18n'
import enGB from './locales/enGB.json'


Vue.use(VueI18n);

Vue.config.productionTip = false


const messages = {
    en: enGB
};

const i18n = new VueI18n({
    locale: 'en',
    messages
});

new Vue({
    router,
    store,
    i18n,
    render: h => h(App)
}).$mount('#app')
