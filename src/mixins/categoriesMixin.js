export default  {
    data() {
        return {
            coreCategories: [
                {
                    name: this.$t('sharedStarships'),
                    routeName: 'starships',
                    image: 'images/millennium-falcon.jpg',
                    alt: "Millennium Falcon"
                },
                {
                    name: this.$t('sharedPlanets'),
                    routeName: 'planets',
                    image: 'images/starwars-planet.png',
                    alt: "Star Wars landscape"
                },
                {
                    name: this.$t('sharedCharacters'),
                    routeName: 'characters',
                    image: 'images/boba-fett.png',
                    alt: "Boba Fett"
                }
            ]
        }
    }
}