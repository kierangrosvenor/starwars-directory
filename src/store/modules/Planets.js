

import { TweenMax, TweenLite, Power3, ScrollToPlugin } from 'gsap/all'

const plugins = [ ScrollToPlugin];



import axios from 'axios'

/**
 * Planets State Object
 * @type {{planet: null, planets: Array, data: null}}
 */
let state = {
    planet: null,
    planets: [],
    data: null
};

/**
 * mutations (set / change the state)
 * @type {{setPlanet(*, *): void, setPlanets(*, *): void}}
 */
let mutations = {

    /**
     * Clear planets array
     * @param state
     */
    clearPlanets(state) {
        state.planets = [];
    },
    /**
     * Clear the current planet
     * @param state
     */
    clearPlanet(state) {
      state.planet = false;
    },
    /**
     * Set the current planet
     * @param state
     * @param data
     */
    setPlanet(state, data) {
        state.planet = data;
    },
    /**
     * Set the planets, apply them
     * @param state
     * @param data
     */
    setPlanets(state, data) {
        state.data = data;
        state.planets
            .push
            .apply(
                state.planets,
                state.data.results
            );
    }
};

/**
 * getters (get data from state)
 * @type {{getPlanet(*): (null|*), getPlanetsData(*): (null|*), getAllPlanets(*): *}}
 */
let getters = {
    /**
     * Get the current planet
     * @param state
     * @returns {null|*}
     */
    getPlanet(state) {
        return state.planet;
    },
    /**
     * Get the SWAPI current pagination info object
     * @param state
     * @returns {null|*}
     */
    getPlanetsData(state) {
        return state.data;
    },
    getAllPlanets(state) {
        return state.planets;
    }
};

/**
 * actions (do stuff, then modify state later)
 * @type {{getPlanet({commit: *}, *=): Promise<void>, getPlanets({commit: *, dispatch: *}, *=): Promise<void>}}
 */
let actions = {
    /**
     * Get one planet by SWAPI api url
     * Set planet state object
     * @param commit
     * @param url
     * @returns {Promise<void>}
     */
    async getPlanet({commit}, url) {
        try {
            let result = await axios.get(url);
            TweenLite.to(window, 0.4, {scrollTo: {x:0, y:0}});
            commit('setPlanet', result.data);
        }
        catch (e) {
            if (process.env.NODE_ENV === "development") {
                console.log(e);
            }
        }
    },
    /**
     * Get planets, dispatch the next page automatically until it reaches the end
     * Append to planets state array
     * @param commit
     * @param dispatch
     * @param params
     * @returns {Promise<void>}
     */
    async getPlanets({commit, dispatch}, params = {url: "https://swapi.dev/api/planets/"}) {
        try {
            let result = await axios.get(params.url);
            commit('setPlanets', result.data);
            if (result.data.next !== null) {
                dispatch('getPlanets', {url: result.data.next});
            }
        }
        catch (e) {
            if (process.env.NODE_ENV === "development") {
                console.log(e);
            }
        }
    }
};

export default {
    state,
    mutations,
    actions,
    getters
}