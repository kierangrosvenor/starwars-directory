
import { TweenMax, TweenLite, Power3, ScrollToPlugin } from 'gsap/all'

const plugins = [ ScrollToPlugin];

import axios from 'axios'
import store from '../';

/**
 * Planets State Object
 * @type {{character: null, characters: Array, data: null}}
 */
let state = {
    character: false,
    characters: [],
    data: null
};

/**
 * mutations (set / change the state)
 * @type {{setCharacter(*, *): void, setCharacters(*, *): void}}
 */
let mutations = {
    clearCharacter(state) {
      state.character = false;
    },
    /**
     * Clear characters array
     * @param state
     */
    clearCharacters(state) {
        state.characters = [];
    },
    /**
     * Set the current character
     * @param state
     * @param data
     */
    setCharacter(state, data) {
        state.character = data;
    },
    /**
     * Set the characters, apply them
     * @param state
     * @param data
     */
    setCharacters(state, data) {
        state.data = data;
        state.characters
            .push
            .apply(
                state.characters,
                state.data.results
            );
    }
};

/**
 * getters (get data from state)
 * @type {{getCharacter(*): (null|*), getCharactersData(*): (null|*), getAllCharacters(*): *}}
 */
let getters = {
    /**
     * Get the current character
     * @param state
     * @returns {null|*}
     */

    getCharacter(state) {
        let species = JSON.parse(JSON.stringify(store.getters.getAllSpecies));
        let character = JSON.parse(JSON.stringify(state.character));



        if(character.species) {
            character.speciesData = species.filter(function (item) {
                return item.url === character.species[0]
            })
        }




        return character;
    },
    /**
     * Get the SWAPI current pagination info object
     * @param state
     * @returns {null|*}
     */
    getCharactersData(state) {
        return state.data;
    },
    /**
     * Get all characters
     * Enrichment of species data from other store
     * @param state
     * @returns {Array}
     */
    getAllCharacters(state) {
        let species = JSON.parse(JSON.stringify(store.getters.getAllSpecies));
        let characters = JSON.parse(JSON.stringify(state.characters));
        characters.forEach(function (character) {
            if (character.species.length > 0) {
                character.speciesData = species.filter(function (specie) {
                    return specie.url === character.species[0]
                })[0];
            }
        });
        return characters;
    }
};

/**
 * actions (do stuff, then modify state later)
 * @type {{getCharacter({commit: *}, *=): Promise<void>, getCharacters({commit: *, dispatch: *}, *=): Promise<void>}}
 */
let actions = {
    /**
     * Get one character by SWAPI api url
     * Set character state object
     * @param commit
     * @param url
     * @returns {Promise<void>}
     */
    async getCharacter({commit}, url) {
        try {
            let result = await axios.get(url);
            TweenLite.to(window, 0.4, {scrollTo: {x:0, y:0}});
            commit('setCharacter', result.data);
        }
        catch (e) {
            if (process.env.NODE_ENV === "development") {
                console.log(e);
            }
        }
    },
    /**
     * Get characters, dispatch the next page automatically until it reaches the end
     * Append to characters state array
     * @param commit
     * @param dispatch
     * @param params
     * @returns {Promise<void>}
     */
    async getCharacters({commit, dispatch}, params = {url: "https://swapi.dev/api/people/"}) {
        try {
            let result = await axios.get(params.url);
            commit('setCharacters', result.data);
            if (result.data.next !== null) {
                dispatch('getCharacters', {url: result.data.next});
            }
        }
        catch (e) {
            if (process.env.NODE_ENV === "development") {
                console.log(e);
            }
        }
    }
};

export default {
    state,
    mutations,
    actions,
    getters
}