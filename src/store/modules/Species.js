

import axios from "axios/index";

/**
 * State for species
 * @type {{speciesData: null, species: Array}}
 */
let state = {
    speciesData: null,
    species: []
};

/**
 * mutations set things
 * @type {{setSpecies(*, *): void}}
 */
let mutations = {
    /**
     * Clear species state array
     * @param state
     */
    clearSpecies(state) {
        state.species = [];
    },
    /**
     * Set the species state array / append to it
     * @param state
     * @param data
     */
    setSpecies(state, data) {
        state.species
            .push
            .apply(
                state.species,
                data.results
            );
    }
};

let actions = {
    async getSpecies({commit, dispatch}, params = {url: "https://swapi.dev/api/species/"}) {
        try {
            let result = await axios.get(params.url);
            commit('setSpecies', result.data);
            if (result.data.next !== null) {
                dispatch('getSpecies', {url: result.data.next});
            }
        }
        catch (e) {
            if (process.env.NODE_ENV === "development") {
                console.log(e);
            }
        }
    }
}

let getters = {

    getAllSpecies(state) {
        return state.species;
    }
};

export default {
    state,
    actions,
    mutations,
    getters
}