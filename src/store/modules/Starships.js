import { TweenMax, TweenLite, Power3, ScrollToPlugin } from 'gsap/all'

const plugins = [ ScrollToPlugin];

import axios from 'axios'

/**
 * Starships State Object
 * @type {{starship: null, starships: Array, data: null}}
 */
let state = {
    starship: false,
    starships: [],
    data: null
};

/**
 * mutations (set the state)
 * @type {{setStarship(*, *): void, setStarships(*, *): void}}
 */
let mutations = {
    /**
     * Clear starships
     * @param state
     */
    clearStarships(state) {
        state.starships = [];
    },
    /**
     * Clear the current starship object
     * @param state
     */
    clearStarship(state) {
      state.starship = false;
    },
    /**
     * Set the current starship
     * @param state
     * @param data
     */
    setStarship(state, data) {
        state.starship = data;
    },
    /**
     * Append to starships state array
     * @param state
     * @param data
     */
    setStarships(state, data) {
        state.data = data;
        state.starships
            .push
            .apply(
                state.starships,
                state.data.results
            );
    }
};

/**
 * getters (get things)
 * @type {{getStarship(*): *, getStarshipsData(*): *, getAllStarships(*): *}}
 */
let getters = {
    /**
     * Get the current starship
     * @param state
     * @returns {null}
     */
    getStarship(state) {
        return state.starship;
    },
    /**
     * Get the current SWAPI pagination object
     * @param state
     * @returns {null}
     */
    getStarshipsData(state) {
        return state.data;
    },
    /**
     * Get an array of starships
     * @param state
     * @returns {Array}
     */
    getAllStarships(state) {
        return state.starships;
    }
};

/**
 * actions (do things)
 * @type {{getStarship({commit: *}, *=): Promise<void>, getStarships({commit: *, dispatch: *}, *=): Promise<void>}}
 */
let actions = {
    /**
     * Gets a starship and commits it to the store
     * @param commit
     * @param url
     * @returns {Promise<void>}
     */
    async getStarship({commit}, url) {
        try {

            let result = await axios.get(url);
            TweenLite.to(window, 0.4, {scrollTo: {x:0, y:0}});
            commit('setStarship', result.data);
        }
        catch (e) {
            if (process.env.NODE_ENV === "development") {
                console.log(e);
            }
        }
    },
    /**
     * Gets starships until next is null
     * Appends them to the starships state array
     * @param commit
     * @param dispatch
     * @param params
     * @returns {Promise<void>}
     */
    async getStarships({commit, dispatch}, params = {url: "https://swapi.dev/api/starships/"}) {
        try {
            let result = await axios.get(params.url);
            commit('setStarships', result.data);
            if (result.data.next !== null) {
                dispatch('getStarships', {url: result.data.next});
            }
        }
        catch (e) {
            if (process.env.NODE_ENV === "development") {
                console.log(e);
            }
        }
    }
};

export default {
    state,
    mutations,
    actions,
    getters
}